'use strict';
const moment = require('moment');

exports.open = (event, context, callback) => {
  console.log("Request with event: " + JSON.stringify(event));

  var result = {
     "message": "Hello! This is a private sample function.",
     "callingTime": moment().format('DD.MM.YYYY hh:mm:ss'),
     "calledWithEventBody": event.body}

  callback(null, result);
}