/* role to be set at the lambda, managing permissions for lambda */
resource "aws_iam_role" "lambda" {
  name = "${var.environment}-sample-function-role"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role.json}"
}

/* lambda itself must be able to assume this role */
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy_attachment" "lambda" {
  policy_arn = "${aws_iam_policy.lambda.arn}"
  role = "${aws_iam_role.lambda.name}"
}

resource "aws_iam_policy" "lambda" {
  policy = "${data.aws_iam_policy_document.lambda.json}"
}

data "aws_iam_policy_document" "lambda" {
  statement {
    sid = "AllowManagingLogging"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "*"]
  }

  statement {
    sid = "AllowUseCloudWatch"
    effect = "Allow"
    actions = [
      "cloudwatch:Describe*",
      "cloudwatch:Get*",
      "cloudwatch:List*",
    ]
    resources = [
      "*"]
  }
}