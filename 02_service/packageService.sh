#!/bin/bash
set -eo pipefail

function packageNodeLambda() {
    local srcDir="${1}"
    local targetFile="${srcDir}/lambda.zip"

    rm -rf "${targetFile}"

    pushd "${srcDir}" > /dev/null
      # cleanup...
      rm -rf node_modules
      rm -rf lambda*
      npm install
      zip --quiet -r "lambda.zip" *
    popd > /dev/null
}

packageNodeLambda "../sample-function"