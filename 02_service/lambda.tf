resource "aws_lambda_function" "sample-function" {
  function_name = "${var.environment}-sample-function"
  filename = "../sample-function/lambda.zip"
  source_code_hash = "${base64sha256(file("../sample-function/lambda.zip"))}"
  handler = "index.open"
  role = "${aws_iam_role.lambda.arn}"
  runtime = "nodejs8.10"
}