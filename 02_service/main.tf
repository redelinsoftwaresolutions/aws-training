provider "aws" {
  region = "${local.region}"
  assume_role {
    # role arn to be assumed automatically created by 01_pipeline-role directory
    role_arn = "arn:aws:iam::595466662242:role/aws-training-pipeline-role"
  }
}