# Training for initial understanding of AWS
This training should help you to get an initial understanding of AWS using terraform.  
There is no programming challenge to be succeeded. It will give you the first 
The basic content is to create a Hello World Lambda function, which is accessible from the internet.  
## pre-requisites
### your developer environment
To be able to deploy and run terraform scripts against your account you have to install some tools.  
These tools can be installed by running the following commands within a unix system (e.g. Ubuntu, MacOS) or Ubuntu@Windows
```
cd 00_prerequisites
./install.sh
```
Following things will be installed:  
1. some unix tools  
2. aws-cli  
3. terraform  
4. swamp
  
### your AWS account
First of all you need a private AWS account. If you haven't got one, register one (no costs will be generated). http://console.aws.amazon.com  
Create the following initial configuration at your account.  
1. create a new user "terraform"  
   1. visit service "IAM" >> "Users"  
   2. create new user named "terraform" with policy "Administrator"  
   3. use "programmatic access" for this user and note AccessKeyId and SecretAccessKey
   4. add a MFA device to this user
   
## the training
basically this training will show you in a few steps how to configure a functional infrastructure based on  
* roles to do things in the right way and avoid doing the wrong things with your application you won't do  
* create a lambda-function which is printing some HelloWorld stuff in JavaScript
it does explicitly not contain  
* access the lambda-function from the internet using an ApplicationLoadBalancer (ALB)
* backend-state storage in S3 bucket. That means if you delete the local tfstate, you will get problems while executing the terraform scripts another time due to duplicate resources.  
* lambda artifact storage in S3 bucket  
* configuring NAT gateway with more usable domain names  

### release the pipeline-role
Run the following commands
```
cd terraform/01_pipeline-role
# initialize terraform execution
terraform init
# switch to workspace for dev environment or create one if not exists
terraform workspace select dev || terraform workspace new dev
# apply the changes to your account
terraform apply -var-file=dev.tfvars
```

### release the service with its resources
Run the following commands
```
cd terraform/02_service
./packageService.sh
# initialize terraform execution
terraform init
# switch to workspace for dev environment or create one if not exists
terraform workspace select dev || terraform workspace new dev
# plan changes to see, what changes will be performed without executing them
terraform plan -var-file=dev.tfvars
# apply the changes to your account
terraform apply -var-file=dev.tfvars
```

### test the lambda in AWS console
1. Go to service "lambda" in AWS console  
2. Click on your deployed lambda function "dev-sample-function"  
3. Click on button "Test" to create a new test execution  
4. Add the following JSON to your Test  
    ```
    {
      "headers": ["application/json"],
      "body": {
        "key1": "value2",
        "key2": "value3"
      }
    }
    ```
5. Click on button "Test" another time to execute the lambda with the given payload.  
6. Result would be a JSON Response, which tells you the current caller time and a result object.

### destroy all resources using terraform
run the following commands from project root to destroy all created resources  
```
cd 01_pipeline-role
terraform destroy -var-file=dev.tfvars
cd ../02_service
terraform destroy -var-file=dev.tfvars
```