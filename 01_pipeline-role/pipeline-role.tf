/*
  with this role you are able to deploy the service afterwards. The role specifies exactly the necessary permissions.
*/
resource "aws_iam_role" "pipeline-role" {
  name = "${local.servicename}-pipeline-role"
  assume_role_policy = "${data.aws_iam_policy_document.assume-pipeline-role.json}"
}

# specifies who is able to assume this role. currently only all users from our account are able to assume, no cross account assuming.
data "aws_iam_policy_document" "assume-pipeline-role" {
  statement {
    sid = "AssumeRole"
    effect = "Allow"
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      ]
    }
    actions = [
      "sts:AssumeRole"]
  }
}

# attach policy to role
resource "aws_iam_role_policy_attachment" "pipeline-role-policies" {
  policy_arn = "${aws_iam_policy.pipeline-role.arn}"
  role = "${aws_iam_role.pipeline-role.name}"
}

# create policy resource
resource "aws_iam_policy" "pipeline-role" {
  policy = "${data.aws_iam_policy_document.pipeline-role.json}"
}

# specifies what are the things to be allowed with this policy
data "aws_iam_policy_document" "pipeline-role" {
  statement {
    sid = "AllowIAMRoles"
    effect = "Allow"
    actions = [
      "iam:*Tag*",
      "iam:AttachRolePolicy",
      "iam:CreatePolicy",
      "iam:CreatePolicyVersion",
      "iam:CreateRole",
      "iam:CreateServiceLinkedRole",
      "iam:DeletePolicy",
      "iam:DeletePolicyVersion",
      "iam:DeleteRole",
      "iam:DeleteRolePolicy",
      "iam:DetachRolePolicy",
      "iam:Get*",
      "iam:List*",
      "iam:PassRole",
      "iam:PutRolePolicy",
      "iam:UpdateAssumeRolePolicy",
    ]
    resources = [
      "*"]
  }

  statement {
    sid = "AllowLambda"
    effect = "Allow"
    actions = [
      "lambda:*"]
    resources = [
      "*"]
  }

  statement {
    sid = "AllowLoadBalancers"
    effect = "Allow"
    actions = [
      "elasticloadbalancing:CreateRule",
      "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteRule",
      "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:Describe*",
      "elasticloadbalancing:ModifyRule",
      "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:SetRulePriorities",
      "elasticloadbalancing:AddTags",
      "elasticloadbalancing:RemoveTags",
      "elasticloadbalancing:DescribeTags",
    ]
    resources = [
      "*"]
  }
}