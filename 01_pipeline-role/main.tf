provider "aws" {
  region = "${local.region}"
}

data "aws_caller_identity" "current" {}