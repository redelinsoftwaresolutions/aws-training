output "pipeline-role-arn" {
  value = "${aws_iam_role.pipeline-role.arn}"
}