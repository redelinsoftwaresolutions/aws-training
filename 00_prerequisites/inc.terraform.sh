#!/bin/bash
TF_BASE="https://releases.hashicorp.com/terraform"
TF_VERSION=$(curl --insecure -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')
TF_FILE=terraform_${TF_VERSION}_darwin_amd64.zip

if [[ "${TF_VERSION_INSTALLED}" != "${TF_VERSION}" ]]; then
  echo "downloading terraform from $TF_BASE/$TF_VERSION/$TF_FILE"
  wget -q "$TF_BASE/$TF_VERSION/$TF_FILE"
  unzip -q ${TF_FILE}
  mv -f terraform /usr/local/bin/
  rm terraform_*
  echo "Installed Terraform with version ${TF_VERSION}"
else
  echo "Terraform already installed in version ${TF_VERSION}"
fi