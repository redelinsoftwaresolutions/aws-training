#!/bin/bash
if [ ! -f /usr/local/bin/swamp ]; then
    wget https://github.com/felixb/swamp/releases/download/v0.9.1/swamp_darwin -O /usr/local/bin/swamp
    chmod 755 /usr/local/bin/swamp
fi