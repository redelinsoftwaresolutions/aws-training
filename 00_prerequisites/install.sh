#!/bin/bash
set -eo pipefail

if [ -z "${AWS_ACCOUNT_ID+x}" ]; then
  echo "enter your aws account-id please to create aliases etc."
  read AWS_ACCOUNT_ID
fi
if [ -z "${AWS_USERNAME+x}" ]; then
  echo "enter your aws username please to create aliases etc."
  read AWS_USERNAME
fi

SCRIPT_NAME=$0
PWD=$(pwd)

JAVA_HOME=$(ls -d1 /Library/Java/JavaVirtualMachines/jdk1.8* | head -1)/Contents/Home

if [[ "$OSTYPE" == "linux"* ]]; then
  $PWD/inc.linux.sh
elif [[ "$OSTYPE" == "darwin"* ]]; then
  $PWD/inc.macos.sh
else
  echo "OS (${OSTYPE}) not supported"
  exit 1
fi

pip3 install virtualenv

# install oh-my-zsh
if [ ! -d ~/.oh-my-zsh ]; then
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi

# install aws cli
$PWD/inc.aws-cli.sh

# install swamp by felixb to support cross account role assumes in aws
$PWD/inc.swamp.sh

# install terraform and read version at this point as well to avoid unncessesary downloads
TF_VERSION=$(curl --insecure -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')
$PWD/inc.terraform.sh

# adding .bashrc
export CREDS_AWS_FILE=~/.aws/credentials

rm ~/.zshrc_local
cat > ~/.zshrc_local <<EOF

export AWS_USERNAME=${AWS_USERNAME}
export AWS_ACCOUNT_ID=${AWS_ACCOUNT_ID}
export JAVA_HOME=${JAVA_HOME}
export EDITOR=$(which vim)
export TF_VERSION_INSTALLED=${TF_VERSION}

mkdir -p ~/.aws

# add initial aws credentials file for user
if [ ! -f ~/.aws/credentials ]; then
    echo "[default]" > ${CREDS_AWS_FILE}
    echo "# aws credentials for YOUR PERSONAL USER" >> ${CREDS_AWS_FILE}
    echo "aws_access_key_id=" >> ${CREDS_AWS_FILE}
    echo "aws_secret_access_key=" >> ${CREDS_AWS_FILE}
    echo "###### Please add aws credentials for YOUR PERSONAL USER ######"
    sleep 5
    vim ${CREDS_AWS_FILE}
fi

if [ ! -f ~/.aws/config ]; then
    echo "[default]" > ~/.aws/config
    echo "region=eu-central-1" >> ~/.aws/config
    echo "output=json" >> ~/.aws/config
    echo "###### Please add aws config ######"
    sleep 5
    vim ~/.aws/config
fi

git config --global core.autocrlf input
git lfs install
cd ~/Documents

EOF

if [ ! -f ~/.zshrc_backup ]; then
    cp ~/.zshrc ~/.zshrc_backup
fi

rm ~/.zshrc
cp ~/.zshrc_backup ~/.zshrc
echo ". ~/.zshrc_local" >> ~/.zshrc
chmod a+x ~/.zshrc*

# adding bash_aliases
cat > ~/.zsh_aliases <<EOF
alias tf='/usr/local/bin/terraform'
# swamping roles like following will be a good idea
# alias swamp-live-admin='swamp -target-profile ${AWS_USERNAME}_live_admin -target-role admin -account ${BU_LIVE_ACCOUNT} -mfa-device arn:aws:iam::${BU_USERS_ACCOUNT}:mfa/${AWS_USERNAME}'
alias provision-update='CURRENT_PWD=\$(pwd) && cd ${PWD} && ${SCRIPT_NAME} && cd \$CURRENT_PWD'
EOF

chmod a+x ~/.zsh_aliases
echo ". ~/.zsh_aliases" >> ~/.zshrc_local

exec zsh