#!/bin/bash
if [[ $(which brew) == "brew not found" ]]; then
  echo "homebrew not found. please install: https://brew.sh/"
  exit 1
fi
set +e
brew update
brew upgrade
brew install wget
brew install python3 git git-lfs unzip vim make jq zsh node
set -e

### sometimes at macos node was not installed successfully.
### If postinstallation failed, you can run
# sudo chown -R $(whoami):admin /usr/local/lib/node_modules/
# brew postinstall node

### If you want to reinstall node run
# brew uninstall --force node
# sudo chown -R $(whoami):wheel /usr/local/share/systemtap
# sudo chown -R $(whoami):admin /usr/local/lib/node_modules/
# brew install node

