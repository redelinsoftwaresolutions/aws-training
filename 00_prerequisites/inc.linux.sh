#!/bin/bash
if [[ $(which apt-get) == "apt-get not found" ]]; then
  echo "apt-get not found. no other package manager is supported yes. Perhaps you can configure one in script"
  exit 1
fi
set +e
apt-get update
apt-get -y install wget
apt-get -y install python3 git git-lfs unzip git vim make jq zsh node
apt-get -y autoclean
apt-get -y autoremove
set -e